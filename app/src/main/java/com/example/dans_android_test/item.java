package com.example.dans_android_test;

public class item {
    private String title;
    private String location;

    public item(String title, String location) {
        this.title = title;
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }
}
