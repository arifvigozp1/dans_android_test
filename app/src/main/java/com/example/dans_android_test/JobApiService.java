package com.example.dans_android_test;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JobApiService {
    @GET("positions.json")
    Call<List<item>> getJobList(
            @Query("description") String description,
            @Query("location") String location,
            @Query("full_time") String fullTime,
            @Query("page") int page
    );
}
