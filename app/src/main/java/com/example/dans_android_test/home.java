package com.example.dans_android_test;// Import statements...

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dans_android_test.JobListAdapter;
import com.example.dans_android_test.item;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class home extends AppCompatActivity {
    private RecyclerView recyclerView;
    private JobListAdapter adapter;
    private CardView loadingCardView;

    private int currentPage = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = findViewById(R.id.recyclerView);
        loadingCardView = findViewById(R.id.loadingCardView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new JobListAdapter();
        recyclerView.setAdapter(adapter);

        fetchJobList();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isLoading && !isLastPage && isLastItemDisplayed(recyclerView)) {
                    currentPage++;
                    fetchJobList();
                }
            }
        });
    }

    private void showLoading() {
        loadingCardView.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        loadingCardView.setVisibility(View.GONE);
    }

    private void fetchJobList() {
        isLoading = true;
        showLoading();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://dev3.dansmultipro.co.id/api/recruitment/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JobApiService apiService = retrofit.create(JobApiService.class);

        Call<List<item>> call = apiService.getJobList(
                "python",
                "berlin",
                "true",
                currentPage
        );

        call.enqueue(new Callback<List<item>>() {
            @Override
            public void onResponse(@NonNull Call<List<item>> call, @NonNull Response<List<item>> response) {
                isLoading = false;
                hideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    List<item> jobList = response.body();

                    if (jobList.isEmpty()) {
                        isLastPage = true;
                    }

                    adapter.addJobs(jobList);
                } else {
                    Toast.makeText(home.this, "Failed to load data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<item>> call, @NonNull Throwable t) {
                isLoading = false;
                hideLoading();

                Toast.makeText(home.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();

                Intent intent = new Intent(home.this, ErrorActivity.class);
                startActivity(intent);

            }
        });
    }

    private boolean isLastItemDisplayed(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        return (visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0;
    }
}
